<?php



require_once "DatabaseObject.php";

class User implements DatabaseObject
{
    private $id;
    private $email;
    private $password;
    private $vorname;
    private $nachname;
    private $plz;
    private $ort;
    private $addresse;
    private $handynummer;
    private $created_at;
    private $updated_at;
    private $admin;

    private $errors;

    public function __construct($email, $password, $vorname, $nachname, $plz, $ort, $addresse, $handynummer, $created_at, $updated_at, $admin=false)
    {
        $this->id = 0;
        $this->email = $email;
        $this->password = $password;
        $this->vorname = $vorname;
        $this->nachname = $nachname;
        $this->plz = $plz;
        $this->ort = $ort;
        $this->addresse = $addresse;
        $this->handynummer = $handynummer;
        $this->created_at = $created_at;
        $this->updated_at = $updated_at;
        $this->admin = $admin;
    }

    public static function logout(){
        unset($_SESSION["userid"]);
        session_destroy();
    }

    public static function checkUser(){
        if(!isset($_SESSION['userid'])) {
            Header("Location: ../login/index.php");
        }
    }

       /**
     * Creates a new object in the database
     * @return integer ID of the newly created object (lastInsertId)
     */
    public function create()
    {
        $db = Database::connect();
        $sql = "INSERT INTO users (email, passwort, vorname, nachname, plz, ort, adresse, handynummer) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->email,$this->password, $this->vorname, $this->nachname,$this->plz, $this->ort, $this->addresse, $this->handynummer));
        Database::disconnect();
        return $db->lastInsertId();

    }

    /**
     * Update an existing object in the database
     * @return boolean true on success
     */
    public function update()
    {
        $db = Database::connect();
        $sql = "UPDATE guest SET name = :name, email = :email, address = :address  WHERE id = :id";
        $stmt = $db->prepare($sql);
        $stmt->execute(array('id' => $this->id, 'name' => $this->name, 'email' => $this->email, 'address' => $this->address));
        Database::disconnect();
    }

    /**
     * Get an object from database
     * @param integer $id
     * @return object single object or null
     */
    public static function get($id)
    {
        $db = Database::connect();
        $sql = "SELECT * FROM users WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        $userData = $stmt->fetch(PDO::FETCH_ASSOC);

        Database::disconnect();

        if ($userData == null) {
            return null;
        } else {
            $user = new User($userData['email'], $userData['passwort'], $userData['vorname'], $userData['nachname'], $userData['plz'], $userData['ort'], $userData['adresse'], $userData['handynummer'], $userData['created_at'], $userData['updated_at'], $userData['admin']);
            $user->id = $userData["id"];
            return $user;
        }
        
    }

    public static function getEmail($email)
    {
        $db = Database::connect();
        $sql = "SELECT * FROM users where email = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($email));
        $userData = $stmt->fetch(PDO::FETCH_ASSOC);

        Database::disconnect();

        if ($userData == null) {
            return null;
        } else {    
            $user = new User($userData['email'], $userData['passwort'], $userData['vorname'], $userData['nachname'], $userData['plz'], $userData['ort'], $userData['adresse'], $userData['handynummer'], $userData['created_at'], $userData['updated_at']);
            $user->id = $userData["id"];
            return $user;
        }
        
    }

    public static function getAddress($address)
    {
        $db = Database::connect();
        $sql = "SELECT * FROM guest where address = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($address));
        $guestData = $stmt->fetch(PDO::FETCH_ASSOC);

        Database::disconnect();

        if ($guestData == null) {
            return null;
        } else {
            $guest = new Guest($guestData['name'], $guestData['email'], $guestData['address']);
            $guest->id = $guestData["id"];
            return $guest;
        }
        
    }

    /**
     * Get an array of objects from database
     * @return array array of objects or empty array
     */
    public static function getAll()
    {

        $credentials = [];
        $db = Database::connect();
        $sql = "SELECT * FROM guest ORDER BY name ASC";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $guestData = $stmt->fetchAll();
        Database::disconnect();

        foreach ($guestData as $d) {
            $guest = new Guest($d['name'], $d['email'], $d['address']);
            $guest->id = $d["id"];
            $credentials[] = $guest;
        }
        return $credentials;
    }

    /**
     * Deletes the object from the database
     * @param integer $id
     */
    public static function delete($id)
    {
        $db = Database::connect();
        $sql = "DELETE FROM guest WHERE id = :id";
        $stmt = $db->prepare($sql);
        $stmt->execute(array('id' => $id));
        Database::disconnect();
    }

     /**
     * Returns the array of error messages
     * @param array array of the error messages
     */
    public function validateNew(){
            
        if(strlen($this->name) == 0){
            $errors[] = "Bitte geben Sie einen Namen ein";   
        }
       
        if(!filter_var($this->email, FILTER_VALIDATE_EMAIL) || strlen($this->email) == 0) {
            $errors[] =  'Bitte geben Sie eine gültige E-Mail-Adresse ein';
        }
       
        if(strlen($this->address) == 0){
            $errors[] =  "Bitte geben Sie eine Adresse ein";
        }
    
        if(!isset($errors)){
            $checkAddress = Guest::getAddress($this->address);
            $checkEmail = Guest::getEmail($this->email);

            if($checkAddress != null && $checkAddress->name == $this->name && $checkAddress->address == $this->address){
                $errors[] = "Dieser Benutzer existiert bereits in unserer Datenbank";     
            }
    
            if($checkEmail != null && !isset($errors)){
                $errors[] =  "Diese E-Mail ist bereits vergeben";
            }  
        }

        return $errors;

    }

     /**
     * Returns the array of error messages
     * @param array array of the error messages
     */
    public function validateUpdate(){
        
        if(strlen($this->name) == 0){
            $errors[] = "Bitte geben Sie einen Namen ein";
        }
       
        if(!filter_var($this->email, FILTER_VALIDATE_EMAIL) || strlen($this->email) == 0) {
            $errors[] =  'Bitte geben Sie eine gültige E-Mail-Adresse ein';
        }
       
        if(strlen($this->address) == 0){
            $errors[] =  "Bitte geben Sie eine Adresse ein";
        }

        if(!$error){
            $gast = new Guest($_POST['name'], $_POST['email'], $_POST['address']);
            $gast->id = $_GET['id'];
            $gast->update();
            header('Location: index.php');
            }
    }

    /**
     * Getter for some private attributes
     * @return mixed $property
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
        return null;
    }

    /**
     * Setter for some private attributes
     * @return mixed $title
     * @return mixed $value
     */
    public function __set($property, $value){
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

    

}