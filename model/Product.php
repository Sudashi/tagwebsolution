<?php

// TODO Crud

require_once "DatabaseObject.php";

class Product implements DatabaseObject
{
    private $id;
    private $name;
    private $price;
    private $description;
    private $category;
    private $productImage;

    private $errors;

    public function __construct($id, $name, $price, $description, $category, $productImage)
    {
        $this->id = 0;
        $this->name = $name;
        $this->price = $price;
        $this->description = $description;
        $this->category = $category;
        $this->productImage = $productImage;
    }
       /**
     * Creates a new object in the database
     * @return integer ID of the newly created object (lastInsertId)
     */
    public function create()
    {
        $db = Database::connect();
        $sql = "INSERT INTO tbl_prodcut (produktID, bestellungID, menge, processed) values(:produktID, :bestellungID, :menge, :processed)";
        $stmt = $db->prepare($sql);
        $stmt->execute(array('produktID' => $this->produktID, 'bestellungID' => $this->bestellungID, 'menge' => $this->menge, 'processed' => $this->processed));
        Database::disconnect();
        return $db->lastInsertId();
    }

    /**
     * Update an existing object in the database
     * @return boolean true on success
     */
    public function update()
    {
        $db = Database::connect();
        $sql = "UPDATE bestellungen SET produktID = :produktID, bestellungID = :bestellungID, menge = :menge, processsed = :processed WHERE id = :id";
        $stmt = $db->prepare($sql);
        $stmt->execute(array('id' => $this->id, 'produktID' => $this->produktID, 'bestellungID' => $this->bestellungID, 'menge' => $this->menge, 'processed' => $this->processed));
        Database::disconnect();
    }

    /**
     * Get an object from database
     * @param integer $id
     * @return object single object or null
     */
    public static function get($id)
    {
        $db = Database::connect();
        $sql = "SELECT * FROM tbl_product WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        $productData = $stmt->fetch(PDO::FETCH_ASSOC);

        Database::disconnect();

        if ($productData == null) {
            return null;
        } else {
            $product = new Product($productData['id'], $productData['name'], $productData['price'], $productData['description'], $productData['category'], $productData['productImage']);
            $product->id = $productData["id"];
            return $product;
        }
        
    }

    /**
     * Get an order and it's corresponding bundles from database
     * @param integer $bestellungID
     * @return array array of orders
     */
    public static function getWholeOrder($bestellungID)
    { 
        $credentials = [];
        $db = Database::connect();
        $sql = "SELECT * FROM bestellungen where bestellungID = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($bestellungID));
        $orderData = $stmt->fetchAll();

        Database::disconnect();
        
        foreach ($orderData as $d) {
            $order = new Order($d['produktID'], $d['bestellungID'], $d['menge'], $d['processed']);
            $order->id = $d["id"];
            $credentials[] = $order;
        }
        return $credentials;
    }


                    
    public static function getProccesed($processed = false)
    {
       
        $credentials = [];
        $db = Database::connect();
        $sql = "SELECT * FROM bestellungen where processed = $processed";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $orderData = $stmt->fetchAll();
        Database::disconnect();

        foreach ($orderData as $d) {
            $order = new Order($d['produktID'], $d['bestellungID'], $d['menge'], $d['processed']);
            $order->id = $d["id"];
            $credentials[] = $order;
        }
        return $credentials;
    }

    /**
     * Get an array of objects from database
     * @return array array of objects or empty array
     */
    public static function getAll()
    {

        $credentials = [];
        $db = Database::connect();
        $sql = "SELECT * FROM tbl_product";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $orderData = $stmt->fetchAll();
        Database::disconnect();

        foreach ($orderData as $d) {
            $product = new Product(0, $d['name'], $d['price'], $d['description'], $d['category'], $d['productImage']);
            $product->id = $d["id"];
            $credentials[] = $product;
        }
        return $credentials;
    }

     /**
     * Get an array of objects from database
     * @return array array of objects or empty array
     */
    public static function getFiltered($filter, $category = null, $sort = null)
    {

        
        $credentials = [];
        $db = Database::connect();
        $sql = "SELECT * FROM tbl_product";
        
        
            $sql .= " WHERE name LIKE '%".$filter."%' ";
        
        if($category){
            $sql .= " AND category LIKE '".$category."' ";
        }
        if($sort){
            $sql .= " ORDER BY price ".$sort;
        }

        $stmt = $db->prepare($sql);
        $stmt->execute();
        $orderData = $stmt->fetchAll();
        Database::disconnect();

        foreach ($orderData as $d) {
            $product = new Product(0, $d['name'], $d['price'], $d['description'], $d['category'], $d['productImage']);
            $product->id = $d["id"];
            $credentials[] = $product;
        }
        return $credentials;
    }

    /**
     * Deletes the object from the database
     * @param integer $id
     */
    public static function delete($id)
    {
        $db = Database::connect();
        $sql = "DELETE FROM bestellungen WHERE id = :id";
        $stmt = $db->prepare($sql);
        $stmt->execute(array('id' => $id));
        Database::disconnect();
    }
    
    /**
     * Getter for some private attributes
     * @return mixed $property
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
        return null;
    }

    /**
     * Setter for some private attributes
     * @return mixed $title
     * @return mixed $value
     */
    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }




}