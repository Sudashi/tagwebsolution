<?php

require_once "CartItem.php";
require_once "Item.php";

class ShoppingCart {
    private $itemList = []; 

    public function __construct($list){
        $this->itemList = $list;
    }
    public function setList($list){
        $this->itemList = $list;
    }

    public function getItems(){
        return $this->itemList;
    }

    public function getIDs(){
        $idArray = [];
        for ($i=0; $i < sizeof($this->itemList); $i++) { 
            if($this->itemList[$i]->getUser() == $_SESSION["userid"]){
                $idArray[] = $this->itemList[$i]->getItem()->getId();
            }
        }
        return $idArray;
    }

}
