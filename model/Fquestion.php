<?php

require_once "DatabaseObject.php";

class Fquestion implements DatabaseObject
{
    private $id;
    private $topic;
    private $detail;
    private $name;
    private $email;
    private $datetime;
    private $view;
    private $reply;

    private $errors;

    public function __construct($topic, $detail, $name, $email, $datetime = 0, $view = 0, $reply = 0)
    {
        $this->id = 0;
        $this->topic = $topic;
        $this->detail = $detail;
        $this->name = $name;
        $this->email = $email;
        $this->datetime = $datetime;
        $this->view = $view;
        $this->reply = $reply;

    }
       /**
     * Creates a new object in the database
     * @return integer ID of the newly created object (lastInsertId)
     */
    public function create()
    {
        if($this->validate()){
        $db = Database::connect();
        $sql = "INSERT INTO fquestions (topic, detail, name, email, datetime) values(:topic, :detail, :name, :email, :datetime)";
        $stmt = $db->prepare($sql);
        $stmt->execute(array('topic' => $this->topic, 'detail' => $this->detail, 'name' => $this->name, 'email' => $this->email, 'datetime' => $this->datetime));
        Database::disconnect();
        return $db->lastInsertId();
        }
        return null;
    }

    public function validate(){
        if(strlen($this->topic)>3&&strlen($this->detail)>3){
            return true;
        }
        return false;
    }

    /**
     * Update an existing object in the database
     * @return boolean true on success
     */
    public function update()
    {
        $db = Database::connect();
        $sql = "UPDATE fquestions SET topic = :topic, detail = :detail, name = :name, email = :email, datetime = :datetime, view = :view, reply = :reply WHERE id = :id";
        $stmt = $db->prepare($sql);
        $stmt->execute(array('id' => $this->id, 'topic' => $this->topic, 'detail' => $this->detail, 'name' => $this->name, 'email' => $this->email, 'datetime' => $this->datetime, 'view' => $this->view, 'reply' => $this->reply));
        Database::disconnect();
    }

    /**
     * Get an object from database
     * @param integer $id
     * @return object single object or null
     */
    public static function get($id)
    {
        $db = Database::connect();
        $sql = "SELECT * FROM fquestions where id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        $fquestionData = $stmt->fetch(PDO::FETCH_ASSOC);

        Database::disconnect();

        if ($fquestionData == null) {
            return null;
        } else {
            $fquestion = new Fquestion($fquestionData['topic'], $fquestionData['detail'], $fquestionData['name'], $fquestionData['email'], $fquestionData['datetime'], $fquestionData['view'], $fquestionData['reply']);
            $fquestion->id = $fquestionData["id"];
            return $fquestion;
        }
        
    }


    /**
     * Get an array of objects from database
     * @return array array of objects or empty array
     */
    public static function getAll()
    {

        $questions = [];
        $db = Database::connect();
        $sql = "SELECT * FROM fquestions";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $fquestionData = $stmt->fetchAll();
        Database::disconnect();

        foreach ($fquestionData as $d) {
            $fquestion = new Fquestion($d['topic'], $d['detail'], $d['name'], $d['email'], $d['datetime'], $d['view'], $d['reply']);
            $fquestion->id = $d["id"];
            $questions[] = $fquestion;
        }
        return $questions;
    }

    /**
     * Deletes the object from the database
     * @param integer $id
     */
    public static function delete($id)
    {
        $db = Database::connect();
        $sql = "DELETE FROM fquestions WHERE id = :id";
        $stmt = $db->prepare($sql);
        $stmt->execute(array('id' => $id));
        Database::disconnect();
    }

    /**
     * Deletes the object from the database
     * @param integer $id
     */
    public static function countAll()
    {
        $db = Database::connect();
        $sql = "SELECT MAX(id) as 'idMax' FROM fquestions";
        $queryID = $db->query($sql);
        $IDmax = $queryID->fetch();
        return $IDmax['idMax'] += 1;
        Database::disconnect();
    }
    
    /**
     * Getter for some private attributes
     * @return mixed $property
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
        return null;
    }

    /**
     * Setter for some private attributes
     * @return mixed $title
     * @return mixed $value
     */
    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }




}