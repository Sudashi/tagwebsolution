<?php

class Item{
    private $id = null;
    private $name = "";
    private $price = 0;

    public function __construct($id, $name, $price){
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
    }
    public function __set($property, $value){            
        if(property_exists($this, $property)){
        $this->$property = $value;    
      }
    }  

    public function getTitle() {
        return $this->name;
    }
    public function getPrice() {
        return $this->price;
    }
    public function getId(){
        return $this->id;
    }
    
  }
