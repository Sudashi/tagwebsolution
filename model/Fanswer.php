<?php

require_once "DatabaseObject.php";
require_once "Fquestion.php";

class Fanswer implements DatabaseObject
{
    private $id;
    private $question_id;
    private $a_name;
    private $a_email;
    private $a_answer;
    private $a_datetime;

    private $errors;

    public function __construct($question_id, $a_name, $a_email, $a_answer, $a_datetime = 0)
    {
        $this->id = 0;
        $this->question_id = $question_id;
        $this->a_name = $a_name;
        $this->a_email = $a_email;
        $this->a_answer = $a_answer;
        $this->a_datetime = $a_datetime;
    }

       /**
     * Creates a new object in the database
     * @return integer ID of the newly created object (lastInsertId)
     */
    public function create()
    {
        if($this->validate()){
        $db = Database::connect();
        $sql = "INSERT INTO fanswer (question_id, a_name, a_email, a_answer) values(:question_id, :a_name, :a_email, :a_answer)";
        $stmt = $db->prepare($sql);
        $stmt->execute(array('question_id' => $this->question_id, 'a_name' => $this->a_name, 'a_email' => $this->a_email, 'a_answer' => $this->a_answer));
        Database::disconnect();
        return $db->lastInsertId();
        }
        return null;
    }

    public function validate(){
        if(strlen($this->a_answer)>3&&Fquestion::get($this->question_id)!=null){
            return true;
        }
        return false;
    }

    /**
     * Update an existing object in the database
     * @return boolean true on success
     */
    public function update()
    {
        $db = Database::connect();
        $sql = "UPDATE fanswer SET question_id = :question_id, a_name = :a_name, a_email = :a_email, a_answer = :a_answer, a_datetime = :a_datetime WHERE a_id = :a_id";
        $stmt = $db->prepare($sql);
        $stmt->execute(array('a_id' => $this->id, 'question_id' => $this->question_id, 'a_name' => $this->a_name, 'a_email' => $this->a_email, 'a_answer' => $this->a_answer, 'a_datetime' => $this->a_datetime));
        Database::disconnect();
    }

    /**
     * Get an object from database
     * @param integer $id
     * @return object single object or null
     */
    public static function get($id)
    {
        $db = Database::connect();
        $sql = "SELECT * FROM fanswer where a_id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        $answerData = $stmt->fetch(PDO::FETCH_ASSOC);

        Database::disconnect();

        if ($answerData == null) {
            return null;
        } else {
            $answer = new Fanswer($answerData['question_id'], $answerData['a_name'], $orderData['a_email'], $orderData['a_answer']);
            $answer->id = $answerData["a_id"];
            $answer->a_datetime = $answerData["a_datetime"];
            return $answer;
        }
        
    }

    /**
     * Get an order and it's corresponding bundles from database
     * @param integer $bestellungID
     * @return array array of orders
     */
    public static function getAllFromQuestion($questionID)
    { 
        $credentials = [];
        $db = Database::connect();
        $sql = "SELECT * FROM fanswer WHERE question_id=?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($questionID));
        $answerData = $stmt->fetchAll();

        Database::disconnect();
        
        foreach ($answerData as $d) {
            $answer = new Fanswer($d['question_id'], $d['a_name'], $d['a_email'], $d['a_answer'], $d['a_datetime']);
            $answer->id = $d["a_id"];
            $credentials[] = $answer;
        }
        return $credentials;
    }
    
    /**
     * Get an array of objects from database
     * @return array array of objects or empty array
     */
    public static function getAll()
    {

        $credentials = [];
        $db = Database::connect();
        $sql = "SELECT * FROM fanswer";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $answerData = $stmt->fetchAll();
        Database::disconnect();

        foreach ($answerData as $d) {
            $answer = new Fanswer($d['question_id'], $d['a_name'], $d['a_email'], $d['a_answer'], $d['a_datetime']);
            $answer->id = $d["a_id"];
            $answer->a_datetime = $d["a_datetime"];
            $credentials[] = $answer;
        }
        return $credentials;
    }

    /**
     * Deletes the object from the database
     * @param integer $id
     */
    public static function delete($id)
    {
        $db = Database::connect();
        $sql = "DELETE FROM fanswer WHERE a_id = :a_id";
        $stmt = $db->prepare($sql);
        $stmt->execute(array('a_id' => $id));
        Database::disconnect();
    }
    
    /**
     * Getter for some private attributes
     * @return mixed $property
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
        return null;
    }

    /**
     * Setter for some private attributes
     * @return mixed $title
     * @return mixed $value
     */
    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}