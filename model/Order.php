<?php

require_once "DatabaseObject.php";

class Order implements DatabaseObject
{
    private $id;
    private $produktID;
    private $bestellungID;
    private $menge;
    private $processed;

    private $errors;

    public function __construct($produktID, $bestellungID, $menge, $processed)
    {
        $this->id = 0;
        $this->produktID = $produktID;
        $this->bestellungID = $bestellungID;
        $this->menge = $menge;
        $this->processed = $processed;
    }
       /**
     * Creates a new object in the database
     * @return integer ID of the newly created object (lastInsertId)
     */
    public function create()
    {
        if($this->validate()){
        $db = Database::connect();
        $sql = "INSERT INTO bestellungen (produktID, bestellungID, menge, processed) values(:produktID, :bestellungID, :menge, :processed)";
        $stmt = $db->prepare($sql);
        $stmt->execute(array('produktID' => $this->produktID, 'bestellungID' => $this->bestellungID, 'menge' => $this->menge, 'processed' => $this->processed));
        Database::disconnect();
        return $db->lastInsertId();
    }
    return null;
    }


    public function validate(){
        if($this->menge>0){
            return true;
        }
        return false;
    }
    /**
     * Update an existing object in the database
     * @return boolean true on success
     */
    public function update()
    {
        $db = Database::connect();
        $sql = "UPDATE bestellungen SET produktID = :produktID, bestellungID = :bestellungID, menge = :menge, processsed = :processed WHERE id = :id";
        $stmt = $db->prepare($sql);
        $stmt->execute(array('id' => $this->id, 'produktID' => $this->produktID, 'bestellungID' => $this->bestellungID, 'menge' => $this->menge, 'processed' => $this->processed));
        Database::disconnect();
    }

    /**
     * Get an object from database
     * @param integer $id
     * @return object single object or null
     */
    public static function get($id)
    {
        $db = Database::connect();
        $sql = "SELECT * FROM bestellungen where id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        $orderData = $stmt->fetch(PDO::FETCH_ASSOC);

        Database::disconnect();

        if ($orderData == null) {
            return null;
        } else {
            $order = new Order($orderData['produktID'], $orderData['bestellungID'], $orderData['menge'], $orderData['processed']);
            $order->id = $orderData["id"];
            return $order;
        }
        
    }

    /**
     * Get an order and it's corresponding bundles from database
     * @param integer $bestellungID
     * @return array array of orders
     */
    public static function getWholeOrder($bestellungID)
    { 
        $credentials = [];
        $db = Database::connect();
        $sql = "SELECT * FROM bestellungen where bestellungID = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($bestellungID));
        $orderData = $stmt->fetchAll();

        Database::disconnect();
        
        foreach ($orderData as $d) {
            $order = new Order($d['produktID'], $d['bestellungID'], $d['menge'], $d['processed']);
            $order->id = $d["id"];
            $credentials[] = $order;
        }
        return $credentials;
    }


                    
    public static function getProccesed($processed = false)
    {
       
        $credentials = [];
        $db = Database::connect();
        if($processed){
            $sql = "SELECT * FROM bestellungen where processed = 1  ORDER BY lastChanged DESC";
        } else{
        $sql = "SELECT * FROM bestellungen where processed = 0 ORDER BY lastChanged DESC";
    }
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $orderData = $stmt->fetchAll();
        Database::disconnect();

        foreach ($orderData as $d) {
            $order = new Order($d['produktID'], $d['bestellungID'], $d['menge'], $d['processed']);
            $order->id = $d["id"];
            $credentials[] = $order;
        }
        return $credentials;
    }
    
    /**
     * Get an array of objects from database
     * @return array array of objects or empty array
     */
    public static function getAll()
    {

        $credentials = [];
        $db = Database::connect();
        $sql = "SELECT * FROM bestellungen";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $orderData = $stmt->fetchAll();
        Database::disconnect();

        foreach ($orderData as $d) {
            $order = new Order($d['produktID'], $d['bestellungID'], $d['menge'], $d['processed']);
            $order->id = $d["id"];
            $credentials[] = $order;
        }
        return $credentials;
    }

    /**
     * Deletes the object from the database
     * @param integer $id
     */
    public static function delete($id)
    {
        $db = Database::connect();
        $sql = "DELETE FROM bestellungen WHERE id = :id";
        $stmt = $db->prepare($sql);
        $stmt->execute(array('id' => $id));
        Database::disconnect();
    }
    
    /**
     * Getter for some private attributes
     * @return mixed $property
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
        return null;
    }

    /**
     * Setter for some private attributes
     * @return mixed $title
     * @return mixed $value
     */
    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

    public static function process($id, $unprocess = false){
        if($unprocess){
            $process = 0;
        } else{
            $process = 1;
        }
        $db = Database::connect();
        $sql = "UPDATE bestellungen SET processed = $process WHERE bestellungID = :id";
        $stmt = $db->prepare($sql);
        $stmt->execute(array('id' => $id));
        Database::disconnect();
    }



}