<!DOCTYPE html>
<html lang="de">

<head>
  <?php 
  session_start();
  require "../../model/User.php";
  ?>  
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Tag Web Solution</title>

  <link rel="shortcut icon" href="../../res/favicon.ico" type="image/x-icon">
  <style>@import url('https://fonts.googleapis.com/css?family=Ubuntu');</style>
  <style>@import url('https://fonts.googleapis.com/css?family=Montserrat');</style>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

  <!-- Custom CSS -->
  <style>
    @import "../../global.css";
    @import "style.css";
  </style>
</head>
<?php

if(isset($_GET['login'])){
  $u = User::getEmail($_POST['email']);

  if($u == null){
    echo "your email was wrong";
  } else {

    if(password_verify($_POST['passwort'], $u->password)){
      $_SESSION['userid'] = $u->id;
      Header("Location: ../../index.php");
    } else {
      echo "your passwird was wrong";
    }

  }

}

?>

<body data-spy="scroll" data-target=".navbar" data-offset="50">
  <!-- Navigation -->
  <nav style="width: 100vw !important;padding: 0px;" class="navbar navbar-expand-lg navbar-light fixed-top">
    <div style="width: 100vw !important;padding: 0px;" class="container-fluid">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div style="width: 100vw !important;" class="collapse navbar-collapse order-2" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="../../index.php">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../login/index.php">Log In</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- Page Content -->
  <div id="main">
    <div class="container-fluid" id="header">
      <h1>Login</h1>
      <hr align="center" />
    </div>
    <div id="controls" class="container-fluid">
      <form method="post" action="?login=1">
    <input type="email" size="40" maxlength="250" placeholder="E-Mail" name="email" required="required"><br>
        <input type="password" id="passwort" name="passwort" placeholder="Passwort" required><br>

        <p>Sie haben noch keinen Account?<br> Klicken Sie <a href="../register/index.php">hier zum Erstellen</a>!</p>

        <input type="submit" value="login" />
      </form>
    </div>
  </div>

  <script>

    // Scroll
    $('.nav-link').click(function () {
      var sectionTo = $(this).attr('href');
      $('html, body').animate({
        scrollTop: $(sectionTo).offset().top
      }, 1500);
    });

    // Active nav
    $(".nav .nav-link").on("click", function () {
      $(".nav").find(".active").removeClass("active");
      $(this).addClass("active");
    });

    // Fill nav color on sroll
    $(function () {
      $(document).scroll(function () {
        var $nav = $("#navbarResponsive");
        $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
        var $nav2 = $("#cart-nav");
        $nav2.toggleClass('scrolled', $(this).scrollTop() > $nav2.height());
      });
    });

</script>

<script type="text/javascript" src="script.js"></script>

</body>
</html>