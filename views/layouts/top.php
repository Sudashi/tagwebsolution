<?php

function contains($haystack, $needle)
{
    return stripos($haystack, $needle) !== FALSE;
}

$path = pathinfo($_SERVER['REQUEST_URI'], PATHINFO_DIRNAME);

?>
<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Tag Web Solution</title>
    <link rel="shortcut icon" href="res/favicon.ico" type="image/x-icon">
    <style>@import url('https://fonts.googleapis.com/css?family=Ubuntu');</style>
    <style>@import url('https://fonts.googleapis.com/css?family=Montserrat');</style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

      <!-- Custom CSS -->
      <style>
      @import "global.css";
      @import "style.css";
    </style>


  </head>
