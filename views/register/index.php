<!DOCTYPE html>
<html lang="de">
<head>
  <?php 
  session_start();
  ?>  
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Tag Web Solution</title>

  <link rel="shortcut icon" href="../../res/favicon.ico" type="image/x-icon">
  <style>@import url('https://fonts.googleapis.com/css?family=Ubuntu');</style>
  <style>@import url('https://fonts.googleapis.com/css?family=Montserrat');</style>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

  <!-- Custom CSS -->
  <style>
    @import "../../global.css";
    @import "style.css";
  </style>
</head>

<?php

  require "../../model/User.php";

  if(isset($_GET['register'])){
    $u = new User($_POST['email'], $_POST['passwort'], $_POST['vorname'], $_POST['nachname'], $_POST['plz'], $_POST['ort'], $_POST['adresse'], $_POST['handynummer'], 0, 0);
    $u->password = password_hash($u->password, PASSWORD_DEFAULT);
    if($u->create()){
      Header('Location: ../login/index.php');
    }
  }

?>

<body data-spy="scroll" data-target=".navbar" data-offset="50">

  <!-- Navigation -->
  <nav style="width: 100vw !important;padding: 0px;" class="navbar navbar-expand-lg navbar-light fixed-top">
    <div style="width: 100vw !important;padding: 0px;" class="container-fluid">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div style="width: 100vw !important;" class="collapse navbar-collapse order-2" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="../index.php">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../views/login/index.php">Log In</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Content -->
  <div id="main">
    <div class="container-fluid" id="header">
      <h1>Register</h1>
      <hr align="center" />
    </div>
    <div id="controls" class="container-fluid">
      <form method="post" action="?register=1">
        <input type="text" name="vorname" placeholder="Vorname" required="required" /><br>
        <input type="text" name="nachname" placeholder="Nachname" required="required" /><br><br>
        <input type="email" size="40" maxlength="250" placeholder="E-Mail" name="email" required="required"><br>
        <input type="text" name="handynummer" placeholder="Handynummer" required="required" /><br><br>
        <input type="password" id="passwort" name="passwort" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" title="Das Passwort muss mindestens eine Zahl, einen Groß- und Kleinbuchstaben sowie mindestends acht Zeichen enthalten" placeholder="Passwort" required><br>
        <div id="message">
          <h3>Passwortkriterien</h3>
          <p id="letter" class="invalid">Einen Kleinbuchstaben</p>
          <p id="capital" class="invalid">Einen Großbuchstaben</p>
          <p id="number" class="invalid">Eine Zahl</p>
          <p id="length" class="invalid">Mindestens 6 Zeichen</p>
          <p id="alphanumeric" class="invalid">Nur alphanumerische Zeichen</p>
        </div>
        <input type="password" size="40" maxlength="250" placeholder="Passwort bestätigen" name="passwort2"><br><br>
        <input type="text" name="plz" placeholder="Postleitzahl" required="required" /><br>
        <input type="text" name="ort" placeholder="Ort" required="required" /><br>
        <input type="text" name="adresse" placeholder="Adresse" required="required" /><br><br>
        <p>Sie haben schon einen Account?<br> Klicken Sie <a href="../views/login/index.php">hier zum Einloggen</a>!</p>
        <input type="submit" value="Registrieren" />
      </form>
    </div>
  </div>

  <script>
    // Scroll
    $('.nav-link').click(function () {
      var sectionTo = $(this).attr('href');
      $('html, body').animate({
        scrollTop: $(sectionTo).offset().top
      }, 1500);
    });
    // Active nav
    $(".nav .nav-link").on("click", function () {
      $(".nav").find(".active").removeClass("active");
      $(this).addClass("active");
    });
    // Fill nav color on sroll
    $(function () {
      $(document).scroll(function () {
        var $nav = $("#navbarResponsive");
        $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
        var $nav2 = $("#cart-nav");
        $nav2.toggleClass('scrolled', $(this).scrollTop() > $nav2.height());
      });
    });
  </script>

  <script type="text/javascript" src="script.js"></script>
  <?php
    require "validiierung.php";
  ?>
</body>
</html>