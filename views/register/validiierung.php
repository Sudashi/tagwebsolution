<?php
    // Determines if the register form should be shown or not
    $showFormular = true;
    
    if(isset($_GET['register'])) {
        $error = false;
        $email = $_POST['email'];
        $passwort = $_POST['passwort'];
        $passwort2 = $_POST['passwort2'];
    
        //Noch keine Checks!
        $vorname = $_POST['vorname'];
        $nachname = $_POST['nachname'];
        $plz = $_POST['plz'];
        $ort = $_POST['ort'];
        $adresse = $_POST['adresse'];
        $handynummer = $_POST['handynummer'];

        if(strlen($vorname) == 0) {
            echo 'Bitte geben Sie einen Benutzername ein<br>';
            $error = true;
        }
    
        if(strlen($vorname) < 3) {
            echo 'Der Benutzername muss mindestens 3 Zeichen lang sein<br>';
            $error = true;
        }

        if(preg_match('/[^a-z]+/i', $vorname)) {
            echo 'Keine Zahlen oder Sonderzeichen im Vornamen';
            $error = true;
        }
    
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            echo 'Bitte geben Sie eine gültige E-Mail-Adresse ein<br>';
            $error = true;
        }

        if(strlen($passwort) == 0) {
            echo 'Bitte geben Sie ein Passwort ein<br>';
            $error = true;
        }

        if(strlen($passwort) < 6) {
            echo 'Das Passwort muss mindestens 6 Zeichen lang sein<br>';
            $error = true;
        }

        if (!preg_match('/[A-Za-z].*[0-9]|[0-9].*[A-Za-z]/', $passwort)){
            echo 'Das Passwort muss Buchstaben sowie Zahlen beeinhalten<br>';
            $error = true;
        }

        if($passwort != $passwort2) {
            echo 'Die Passwörter müssen übereinstimmen<br>';
            $error = true;
        }

        $user1 = new User($email, $passwort, $vorname, $nachname, $plz, $ort, $adresse, $handynummer);
        
        if(!$error) { 
            $statement = $user1->pdo->prepare("SELECT * FROM users WHERE email = :email");
            $result = $statement->execute(array('email' => $email));
            $user = $statement->fetch();
            
            if($user !== false) {
                echo 'Diese E-Mail-Adresse ist bereits vergeben<br>';
                $error = true;
            }
            
            $statement2 = $user1->pdo->prepare("SELECT * FROM users WHERE handynummer = :handynummer");
            $result2 = $statement2->execute(array('handynummer' => $handynummer));
            $user2 = $statement2->fetch();
            
            if($user2 !== false) {
                echo 'Dieser Benutzername ist bereits vergeben<br>';
                $error = true;
            }

            if(isset($_POST["checkJS"])){
                $error = $_POST["checkJS"];
                if ($error) {
                    echo "JS mag dich nicht... sorry!";    
                }
            }
        }
        
        if(!$error) {    
            if($user1->create()) {
                // Header('Location: ../login/index.php');
            } else {
                echo 'Beim Abspeichern ist leider ein Fehler aufgetreten<br>';
        }

    } 





}