var myInput = document.getElementById("passwort");
var letter = document.getElementById("letter");
var capital = document.getElementById("capital");
var number = document.getElementById("number");
var length = document.getElementById("length");
var alphanumeric = document.getElementById("alphanumeric");

// When the user clicks on the password field, show the message box
myInput.onfocus = function() {
    document.getElementById("message").style.display = "block";
}

// When the user clicks outside of the password field, hide the message box
myInput.onblur = function() {
    document.getElementById("message").style.display = "none";
}

var error = [true, true, true, true, true];

// When the user starts to type something inside the password field
myInput.onkeyup = function() {

  // Validate lowercase letters
  var lowerCaseLetters = /[a-z]/;
  var everythingIsBad = false;

  if(myInput.value.match(lowerCaseLetters)) {  
    letter.classList.remove("invalid");
    letter.classList.add("valid");
    error[0] = false;
  } else {
    letter.classList.remove("valid");
    letter.classList.add("invalid");
    error[0] = true;
  }

  // Validate capital letters
  var upperCaseLetters = /[A-Z]/g;

  if(myInput.value.match(upperCaseLetters)) {  
    capital.classList.remove("invalid");
    capital.classList.add("valid");
    error[1] = false;
  } else {
    capital.classList.remove("valid");
    capital.classList.add("invalid");
    error[1] = true;
  }

  // Validate numbers
  var numbers = /[0-9]/g;
  if(myInput.value.match(numbers)) {  
    number.classList.remove("invalid");
    number.classList.add("valid");
    error[2] = false;
  } else {
    number.classList.remove("valid");
    number.classList.add("invalid");
    error[2] = true;
  }

  // Validate length
  if(myInput.value.length >= 6) {
    length.classList.remove("invalid");
    length.classList.add("valid");
    error[3] = false;
  } else {
    length.classList.remove("valid");
    length.classList.add("invalid");
    error[3] = true;
  }

  var letters = /^[0-9a-zA-Z]+$/;

  if(myInput.value.match(letters)){
    alphanumeric.classList.remove("invalid");
    alphanumeric.classList.add("valid");
    error[4] = false;
  } else {
    alphanumeric.classList.remove("valid");
    alphanumeric.classList.add("invalid");
    error[4] = true;
  }

  for(var i = 0; i<error.length; i++){
    if(error[i]){
      everythingIsBad = true;
      document.getElementById("checkJS").checked = true;
      break;
    } else {
      document.getElementById("checkJS").checked = false;
    }
  }

}