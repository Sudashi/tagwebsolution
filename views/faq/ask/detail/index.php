<!DOCTYPE html>

<html lang="de">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Tag Web Solution</title>

    <link rel="shortcut icon" href="../../../../res/favicon.ico" type="image/x-icon">
    <style>@import url('https://fonts.googleapis.com/css?family=Ubuntu');</style>
    <style>@import url('https://fonts.googleapis.com/css?family=Montserrat');</style>


    <!-- Custom CSS -->

    <style>
      @import "../../../../global.css";
      @import "style.css";
    </style>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
 </head>



  <body data-spy="scroll" data-target=".navbar" data-offset="50">
     <!-- Navigation -->
  <nav style="width: 100vw !important;padding: 0px;" class="navbar navbar-expand-lg navbar-light fixed-top">
    <div style="width: 100vw !important;padding: 0px;" class="container-fluid">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      </button>    

      <div style="width: 100vw !important;" class="collapse navbar-collapse order-2" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="../../../../index.php">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../../../../views/shop/index.php">Shop</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="../../index.php">FAQ</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>				

	<!-- Page Content -->
      <?php


//Requires the models
require "../../../../model/Fquestion.php";
require "../../../../model/Fanswer.php";

//Uses DIE if question id is not set

if(!(isset($_GET['id']))){
  die("Question not found");
} else {
  $id = $_GET['id'];
}

//Creates models
$question = Fquestion::get($id);

if(!(isset($question))){
die('Error');
}
$answers = Fanswer::getAllFromQuestion($id);

?>
	<div id="quest">
		<div class="row">
		  <div class="col-lg-8" id="ask">
			<div class="contain">
				<h1><?php echo $question->topic;?></h1>
				<hr align="left">
				<p><?php echo $question->detail; ?></p>
        <form method="get" action="../../questions/index.php">
        <button style="width: 250px !important;margin-top:40px !important;" name="id" value="<?php echo $_GET['id']?>">Antworten</button>
        </form>
      </div>
	  </div>
		</div>
	</div>

	<div class="faq">
		<div id="answers">
			<div class="row">
			  <?php
          foreach ($answers as $a){
        ?>
			  <div class="col-lg-6">
				<div class="contain">
					<h1><?php echo $a->a_name; ?></h1>
					<hr align="left">
        <p><?php echo $a->a_answer; ?></p>
        
          <?php
            $timestamp = $a->a_datetime;
            echo "<p class='date'>".date("F jS, Y", strtotime($timestamp))."</p>";
          ?>

        </div>
			  </div>
      <?php 
      }
      ?>
		</div>
	</div>
  <script>

	// Make every odd element look different

		$(".question:odd").addClass('question2');

        // Scroll

        $('.nav-link').click(function() {

            var sectionTo = $(this).attr('href');

            $('html, body').animate({

                scrollTop: $(sectionTo).offset().top

            }, 1500);

        });

        // Active nav

        $(".nav .nav-link").on("click", function(){

            $(".nav").find(".active").removeClass("active");

            $(this).addClass("active");

        });

		
		// Fill nav color on sroll

		$(function () {

		  $(document).scroll(function () {

			var $nav = $("#navbarResponsive");

			$nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());

		  });

		});



    </script>
  </body>
</html>

