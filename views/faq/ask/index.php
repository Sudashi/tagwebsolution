<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Tag Web Solution</title>

    <link rel="shortcut icon" href="../../../res/favicon.ico" type="image/x-icon">
    <style>@import url('https://fonts.googleapis.com/css?family=Ubuntu');</style>
    <style>@import url('https://fonts.googleapis.com/css?family=Montserrat');</style>

    <!-- Custom CSS -->

    <style>
      @import "../../../global.css";
      @import "style.css";
    </style>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  </head>

  <body data-spy="scroll" data-target=".navbar" data-offset="50">
	<!-- Navigation -->
  <nav style="width: 100vw !important;padding: 0px;" class="navbar navbar-expand-lg navbar-light fixed-top">
    <div style="width: 100vw !important;padding: 0px;" class="container-fluid">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      </button>    

      <div style="width: 100vw !important;" class="collapse navbar-collapse order-2" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
						<a class="nav-link" href="../../../index.php">Home
							<span class="sr-only">(current)</span>
						</a>
					</li>
					<?php

            //Requires models
            require "../../../model/Fquestion.php";


            session_start();
            if(isset($_SESSION['userid'])) {
              ?>
              <li class="nav-item">
                <a class="nav-link active" href="../../shop/index.php">Shop</a>
              </li>

              <?php 
              }  

              $questionCount = sizeof(Fquestion::getAll());
        
              
						
            
              ?>

					<li class="nav-item">
						<a class="nav-link active" href="../index.php">FAQ</a>
					</li>
        </ul>
      </div>
    </div>
  </nav>

	<!-- Bild-Slider -->
    <div id="home" class="carousel slide" data-ride="carousel" data-interval="false">
      <ul class="carousel-indicators">	
        <li data-target="#home" data-slide-to="0" class="active"></li>
				<?php
				for ($x = 1; $x < $questionCount/4; $x++) {
			?>
        <li data-target="#home" data-slide-to="<?php echo $x; ?>"></li>
				<?php
				}
				?>
      </ul>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <div class="row">	
             	<?php
					// Gets every row from fquestions			         

         $questions = Fquestion::getAll();

			
					// Start looping table row
					$counting = 1;
					foreach ($questions as $q){
					if($counting%5 != 0){
				?>	
			  <div class="col-lg-6">
				<div class="question">
					<a href="detail/index.php?id=<?php echo $q->id;?>">
						<h1><?php echo $q->topic; ?></h1>
						<hr align="left">
						<p><?php echo $q->detail; ?></p>
					</a>
				</div>
			  </div>
			  <?php
			}
			else{
				?>
				 			</div>
        </div>
				<div class="carousel-item">
          <div class="row">	
				<div class="col-lg-6">
					<div class="question">
						<a href="detail/index.php?id=<?php echo $q->id;?>">
							<h1><?php echo $q->topic; ?></h1>
							<hr align="left">
							<p><?php echo $q->detail; ?></p>
						</a>
					</div>
					</div>


<?php
		$counting++;}
			$counting++;
			}?> 
			</div>
    </div>
       </div>
      </div>
      <a class="carousel-control-prev" href="#home" data-slide="prev">
        <span class="carousel-control-prev-icon"></span>
      </a>
      <a class="carousel-control-next" href="#home" data-slide="next">
        <span class="carousel-control-next-icon"></span>
      </a>
    </div>
    <script>


		// Make every odd element look different

		$(".question:odd").addClass('question2');





        // Scroll

        $('.nav-link').click(function() {

            var sectionTo = $(this).attr('href');

            $('html, body').animate({

                scrollTop: $(sectionTo).offset().top

            }, 1500);

        });



        // Active nav

        $(".nav .nav-link").on("click", function(){

            $(".nav").find(".active").removeClass("active");

            $(this).addClass("active");

        });

		

		// Fill nav color on sroll

		$(function () {

		  $(document).scroll(function () {

			var $nav = $("#navbarResponsive");

			$nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());

		  });

		});
    </script>
  </body>
</html>