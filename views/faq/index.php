<?php
//include '../layouts/top.php';
?>

<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Tag Web Solution</title>

    <link rel="shortcut icon" href="../../res/favicon.ico" type="image/x-icon">
    <style>@import url('https://fonts.googleapis.com/css?family=Ubuntu');</style>
    <style>@import url('https://fonts.googleapis.com/css?family=Montserrat');</style>

    <!-- Custom CSS -->
    <style>
      @import "../../global.css";
      @import "style.css";
    </style>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  </head>

  <body data-spy="scroll" data-target=".navbar" data-offset="50">
    <!-- Navigation -->
    <nav style="width: 100vw !important;padding: 0px;" class="navbar navbar-expand-lg navbar-light fixed-top">
      <div style="width: 100vw !important;padding: 0px;" class="container-fluid">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>    

        <div style="width: 100vw !important;" class="collapse navbar-collapse order-2" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="../../index.php">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            
            <?php
              session_start();
              if(isset($_SESSION['userid'])) {
            ?>

            <li class="nav-item">
              <a class="nav-link active" href="../shop/index.php">Shop</a>
            </li>

            <?php 
              }
            ?>

            <li class="nav-item">
              <a class="nav-link active" href="#">FAQ</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->

	  <div id="faq">
      <div class="row" style="height: 100vh !important;">
        <div class="col-lg-6" id="ask">
          <div class="contain">
            <div id="questions_image"></div>
            <p>Sie haben ein Problem? Unsere Community hat wahrscheinlich schon eine Antwort für Sie.</p>
            <button onclick="window.location.href='ask/index.php';">Stöbern</button>
          </div>
        </div>
        <div class="col-lg-6" id="questions">
          <div class="contain">
            <div id="ask_image"></div>
            <p>Nichts gefunden? Dann zögern Sie nicht und stellen Sie unserer Community eine Frage.</p>
            <button onclick="window.location.href='questions/index.php';">Fragen</button>
          </div>
		    </div>
		  </div>
	  </div>

    <script>
        // Scroll
        $('.nav-link').click(function() {
            var sectionTo = $(this).attr('href');
            $('html, body').animate({
                scrollTop: $(sectionTo).offset().top
            }, 1500);
        });

        // Active nav
        $(".nav .nav-link").on("click", function(){
            $(".nav").find(".active").removeClass("active");
            $(this).addClass("active");
        });
  
        // Fill nav color on sroll
    		$(function () {
    		  $(document).scroll(function () {
            var $nav = $("#navbarResponsive");
			      $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
  	  	  });
    		});
    </script>

  </body>
</html>