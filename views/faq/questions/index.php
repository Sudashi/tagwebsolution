<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Tag Web Solution</title>

    <link rel="shortcut icon" href="../../../res/favicon.ico" type="image/x-icon">
    <style>@import url('https://fonts.googleapis.com/css?family=Ubuntu');</style>
    <style>@import url('https://fonts.googleapis.com/css?family=Montserrat');</style>

    <!-- Custom CSS -->
    <style>
      @import "../../../global.css";
      @import "style.css";
    </style>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

  <?php

  require_once '../../../model/User.php';
  require_once '../../../model/Fanswer.php';
  require_once '../../../model/Fquestion.php';

  session_start();

  if(!isset($_SESSION['userid'])) {
     die('Bitte zuerst <a href="../../views/login/index.php">einloggen</a>.');
   }

  if(isset($_POST['detail'])){
    $detail = $_POST['detail'];
    $userid = $_SESSION['userid'];

    $user = User::get($userid);
    $name = $user->vorname." ".$user->nachname;

    //create date time 
    $datetimes=date("d/m/y h:i:s");

    if(!isset($_GET["id"]) && isset($_POST['topic'])){
      $topic = $_POST['topic'];
      $max = Fquestion::countAll();
      $fquestion = new Fquestion($topic, $detail, $name, $user->email);
      $fquestion->create();
      echo '<script>window.location = "../ask/detail/index.php?id='.$max.'"</script>';
    } else{
      $fanswer = new Fanswer($_GET['id'], $name, $user->email,$detail);
      $fanswer->create();
      echo '<script>window.location = "../ask/detail/index.php?id='.$_GET["id"].'"</script>';
    }
  }

  ?>

  </head>

  <body data-spy="scroll" data-target=".navbar" data-offset="50">
  <!-- Navigation -->
  <nav style="width: 100vw !important;padding: 0px;" class="navbar navbar-expand-lg navbar-light fixed-top">
    <div style="width: 100vw !important;padding: 0px;" class="container-fluid">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>    

      <div style="width: 100vw !important;" class="collapse navbar-collapse order-2" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="../../../index.php">Home</a>
          </li>
          <?php
            if(isset($_SESSION['userid'])) {
           ?>
          <li class="nav-item">
            <a class="nav-link active" href="../../shop/index.php">Shop</a>
          </li>
          <?php 
            }
          ?>
          <li class="nav-item">
            <a class="nav-link" href="../index.php">FAQ</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

	<!-- Page Content -->
	<div>
		<div class="row" style="display: table; height: 100vh;">
		  <div class="col-lg-8" id="ask" style="display: table-cell; vertical-align: middle;">
        <div class="contain">
          <form method="post">
            <?php
              if(!isset($_GET["id"])){
            ?>
            <input id="question" type="text" placeholder="Frage" name="topic" /><br>
            <?php
              }
            ?>
            <input id="explanation" type="text" placeholder="Genauere Erklärung" name="detail" /><br>
            <button type="Submit">
            <?php
            if(!isset($_GET["id"])){
              echo "Frage stellen";
            } else {
              echo "Antworten";
            }
            ?>
            </button>
          </form>
        </div>
		  </div>
		</div>
	</div>

	<script>

    // Make every odd element look different
    $(".question:odd").addClass('question2');
  
    // Scroll
    $('.nav-link').click(function() {
      var sectionTo = $(this).attr('href');
      $('html, body').animate({
        scrollTop: $(sectionTo).offset().top
      }, 1500);
    });

    // Active nav
    $(".nav .nav-link").on("click", function(){
      $(".nav").find(".active").removeClass("active");
      $(this).addClass("active");
    });

		// Fill nav color on sroll
    $(function () {
      $(document).scroll(function () {
        var $nav = $("#navbarResponsive");
        $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
      });
		});

  </script>

 </body>
</html>