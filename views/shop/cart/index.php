<?php 
session_start();
require_once "../../../model/Item.php";
require_once "../../../model/User.php";
require_once "../../../model/Shopping_Cart.php";
require_once "../../../model/CartItem.php";
require_once "../../../model/Order.php";
User::checkUser();
$userID = $_SESSION['userid'];

if(!empty($_COOKIE["shopping_cart"])){
  $shoppingCartContent = unserialize($_COOKIE["shopping_cart"])->getItems();

  if(isset($_POST["purchase"])){
    $total = 0;

    foreach($shoppingCartContent as $keys => $values){

      if($values->getUser() == $userID){
        $quantity = $values->getQuantity();
        $productID = $values->getItem()->getId();
        $total += $quantity * $values->getItem()->getPrice();

        $order = new Order($productID, $userID, $quantity, 0);
        $order->create();
      }

    }
    setcookie("shopping_cart", "", time() - 3600, "/");

    echo "<script>alert('Die Bestellung wurde abgeschickt!');setTimeout(function (){window.location.href = '../../shop/index.php';}, 500);</script>";
	}

	if(isset($_GET["action"])){
    if($_GET["action"] == "delete"){

      foreach($shoppingCartContent as $keys => $values){

        if($values->getItem()->getId() == $_GET["id"]){
          unset($shoppingCartContent[$keys]);
          $shoppingCart = unserialize($_COOKIE["shopping_cart"]);
          $shoppingCart->setList($shoppingCartContent);
          setcookie("shopping_cart", serialize($shoppingCart), time() + (86400 * 30), "/");
        }

      }
    }
  }

}

if(isset($_POST["logout"])){
  User::logout();
  Header("Location: ../../../views/login/index.php");
}

$count = 0;

?>
<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>Tag Web Solution</title>

    <link rel="shortcut icon" href="../../../res/favicon.ico" type="image/x-icon">
    <style>@import url('https://fonts.googleapis.com/css?family=Ubuntu');</style>
    <style>@import url('https://fonts.googleapis.com/css?family=Montserrat');</style>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

    <!-- Custom CSS -->
    <style>
      @import "../../../global.css";
      @import "style.css";
    </style>
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="50">

    <!-- Navigation -->
    <nav style="width: 100vw !important;padding: 0px;" class="navbar navbar-expand-lg navbar-light fixed-top">
      <div style="width: 100vw !important;padding: 0px;" class="container-fluid">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div id="cart-nav" class="navbar-collapse collapse order-1">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                <a style="white-space: nowrap" class="nav-link" href="">Korb</a>
                </li>
            </ul>
        </div>        
        <div style="width: 100vw !important;" class="collapse navbar-collapse order-2" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="../../../index.php">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" href="../index.php">Shop</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../../../views/faq/index.php">FAQ</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div id="main">
        <div class="container-fluid" id="header">
            <h1>Ihr Warenkorb</h1>
            <hr align="left" />
            <p>Überprüfen Sie bitte Ihre Bestellung</p>
        </div>
        <div class="container-fluid">
            <div id="cart" class="row">
                <div class="col-lg-12" style="overflow: scroll;">
                    <table>
                        <tr>
                            <th>Produktname</th>
                            <th>Stück</th>
                            <th>Stückpreis</th>
                            <th>Gesamt</th>
                            <th>Aktion</th>
                        </tr>
                        <?php
                        if(!empty($_COOKIE["shopping_cart"])){
                          $shoppingCartContent = unserialize($_COOKIE["shopping_cart"])->getItems();
                          $total = 0;
                          foreach($shoppingCartContent as $keys => $values)
                          {
                            if($values->getUser() == $userID){
						            ?>
                        <tr>
                            <td><?= $values->getItem()->getTitle(); ?></td>
                            <td><?= $values->getQuantity(); ?></td>
                            <td><?= $values->getItem()->getPrice(); ?>€</td>
                            <td><?= number_format($values->getQuantity() * $values->getItem()->getPrice(), 2);?>€</td>
                            <td><a href="index.php?action=delete&id=<?= $values->getItem()->getId(); ?>"><span class="text-danger">Entfernen</span></a></td>
                        </tr>
                        <?php
								            $total += ($values->getQuantity() * $values->getItem()->getPrice());
                          }
                        }
                        ?>
                        <tr>
                          <td colspan="4" align="right">Summe</td>
                          <td align="left"><?= number_format($total, 2); ?>€</td>
                        </tr>
                        <?php
                        }
                        ?>
                    </table>
                </div>
                <div class="col-lg-12">
                <form method="post">
                    <input type="checkbox" name="data" value="Daten" required> Ich akzeptiere hiermit die AGB.
                    <button style="margin-top: 3vh;float:right;" name="purchase">Bestellung senden</button>
                </form>
                </div>
            </div>
        </div>
    </div>
    <?php
    require_once "../../../footer.php";
    ?>

    <script>

      // Scroll
      $('.nav-link').click(function() {
        var sectionTo = $(this).attr('href');
        $('html, body').animate({
          scrollTop: $(sectionTo).offset().top
        }, 1500);
      });

    // Active nav
    $(".nav .nav-link").on("click", function(){
      $(".nav").find(".active").removeClass("active");
        $(this).addClass("active");
      });
    
    // Fill nav color on sroll
    $(function () {
      $(document).scroll(function () {
        var $nav = $("#navbarResponsive");
        $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
        var $nav2 = $("#cart-nav");
        $nav2.toggleClass('scrolled', $(this).scrollTop() > $nav2.height());
      });
    });

  </script>

  </body>
</html>