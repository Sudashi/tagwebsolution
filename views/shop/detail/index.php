<!DOCTYPE html>
<html lang="de">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Tag Web Solution</title>
<link rel="shortcut icon" href="../../../res/favicon.ico" type="image/x-icon">

<style>@import url('https://fonts.googleapis.com/css?family=Ubuntu');</style>
<style>@import url('https://fonts.googleapis.com/css?family=Montserrat');</style>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

<!-- Custom CSS -->
<style>
    @import "../../../global.css";
    @import "style.css";
</style>

</head>
<?php
session_start();
require_once "../../../model/User.php";
require_once "../../../model/Item.php";
require_once "../../../model/Shopping_Cart.php";
require_once "../../../model/CartItem.php";
require_once "../../../model/Product.php";
User::checkUser();
$count = 0;
$btnMessage = "Kaufen";
?>
<body data-spy="scroll" data-target=".navbar" data-offset="50">
<!-- Navigation -->
<nav style="width: 100vw !important;padding: 0px;" class="navbar navbar-expand-lg navbar-light fixed-top">
    <div style="width: 100vw !important;padding: 0px;" class="container-fluid">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive,#cart-nav" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>    
        <div id="cart-nav" class="collapse navbar-collapse order-1">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a style="white-space: nowrap" class="nav-link" href="../cart/index.php">Korb</a>
                </li>
            </ul>
        </div>
    <div style="width: 100vw !important;" class="collapse navbar-collapse order-2" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="../../../index.php">Home
                    <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="../index.php">Shop</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="../../../views/faq/index.php">FAQ</a>
            </li>
        </ul>
    </div>
    </div>
</nav>

<!-- Page Content -->
<div id="main">
    <div class="row" id="outer">
    <?php 
    if(isset($_POST["add_to_cart"])){
        $btnMessage = "OK ✔️";

        if(isset($_COOKIE["shopping_cart"])){
            $shoppingCartContent = unserialize($_COOKIE["shopping_cart"]);

            if(!in_array($_GET["id"], $shoppingCartContent->getIDs())){
                $shoppingCartContent = $shoppingCartContent->getItems();
                $product = Product::get($_GET['id']);

                if($product != null){
                    $item = new Item($product->id, $product->name, $product->price);	
                    $cartItem = new CartItem($item, $_POST["quantity"], $_SESSION["userid"]);
                    
                    array_push($shoppingCartContent, $cartItem);
                    $shoppingCart = unserialize($_COOKIE["shopping_cart"]);
                    $shoppingCart->setList($shoppingCartContent);
                    setcookie("shopping_cart", serialize($shoppingCart), time() + (86400 * 30), "/");
                }
            } else {
                foreach($shoppingCartContent->getItems() as $keys => $values){
                    if($values->getItem()->getId() == $_GET["id"]){
                        $values->setQuantity($values->getQuantity() + $_POST["quantity"]);
                    }
                }
                $shoppingCart = unserialize($_COOKIE["shopping_cart"]);
                $shoppingCart->setList($shoppingCartContent->getItems());
                setcookie("shopping_cart", serialize($shoppingCart), time() + (86400 * 30), "/");
            }
        } else {
            $product = Product::get($_GET['id']);
            if($product != null){
                $item = new Item($product->id, $product->name, $product->price);	
                $cartItem = new CartItem($item, $_POST["quantity"], $_SESSION["userid"]);
                    
                $itemList = array($cartItem);
                $shoppingCart = new ShoppingCart($itemList);
                setcookie("shopping_cart", serialize($shoppingCart), time() + (86400 * 30), "/");			
            }
        }
    }
    
    $product = Product::get($_GET['id']);
    if($product != null){
            ?>

            <div class="col-lg-7">
                <div id="img_container">
                    <img src="data:image/jpeg;base64,<?php echo base64_encode($product->productImage); ?>" width="90%" />
                </div>
            </div>
            <div class="col-lg-5" style="overflow: scroll !important;">
                <div id="details" class="row">
                    <h1><?php echo $product->name; ?></h1>
                    <hr align="left" />
                    <p><?php echo $product->description; ?></p>
                    <form method="post" action="index.php?action=add&id=<?php echo $product->id; ?>">
                        <div class="row" id="det">
                            <div class="col-lg-5">
                                <p id="price"><?php echo $product->price; ?>€</p>
                            </div>
                            <div class="col-lg-3">
                                <input type="number" name="quantity" value="0" />
                            </div>
                            <div class="col-lg-4" style="padding: 0px;">
                                <input style="width: 80%;margin-right:20px;" type="submit" name="add_to_cart" value="<?php echo $btnMessage ?>" />
                            </div>
                        </div>                       
                    </form>
                </div>
            </div>
            <?php
            }
            ?>
        </div>
    </div>

    <script>


        // Scroll


        $('.nav-link').click(function() {


            var sectionTo = $(this).attr('href');


            $('html, body').animate({


                scrollTop: $(sectionTo).offset().top


            }, 1500);


        });





        // Active nav


        $(".nav .nav-link").on("click", function(){


            $(".nav").find(".active").removeClass("active");


            $(this).addClass("active");


        });


		


        // Fill nav color on sroll


        $(function () {


          $(document).scroll(function () {


          var $nav = $("#navbarResponsive");


          $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());


          var $nav2 = $("#cart-nav");


          $nav2.toggleClass('scrolled', $(this).scrollTop() > $nav2.height());


          });


        });





    </script>





  </body>





</html>


