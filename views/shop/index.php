<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Tag Web Solution</title>

    <link rel="shortcut icon" href="../../res/favicon.ico" type="image/x-icon">

    <style>@import url('https://fonts.googleapis.com/css?family=Ubuntu');</style>
    <style>@import url('https://fonts.googleapis.com/css?family=Montserrat');</style>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

    <!-- Custom CSS -->
    <style>
        @import "../../global.css";
        @import "style.css";
    </style>

</head>

<?php 
    session_start();
    require_once "../../model/Item.php";
	require_once "../../model/User.php";
	require_once "../../model/Shopping_Cart.php";
	require_once "../../model/CartItem.php";
	User::checkUser();
	$count = 0;
?>

<body data-spy="scroll" data-target=".navbar" data-offset="50">

<!-- Navigation -->
<nav style="width: 100vw !important;padding: 0px;" class="navbar navbar-expand-lg navbar-light fixed-top">
    <div style="width: 100vw !important;padding: 0px;" class="container-fluid">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div id="cart-nav" class="navbar-collapse collapse order-1">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a style="white-space: nowrap" class="nav-link" href="cart/index.php">Korb</a>
                </li>
            </ul>
        </div>        
        
        <div style="width: 100vw !important;" class="collapse navbar-collapse order-2" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="../../index.php">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                
                <?php
                if(isset($_SESSION['userid'])) {
                ?>
                
                <li class="nav-item">
                    <a class="nav-link active" href="">Shop</a>
                </li>

                <?php 
                }
                ?>

            <li class="nav-item">
                <a class="nav-link" href="../../views/faq/index.php">FAQ</a>
    	    </li>

            </ul>
        </div>
    </div>
</nav>

<!-- Page Content -->
<div id="main">
    <div class="container-fluid" id="header">
        <h1>TWS Online Shop</h1>
        <hr align="left" />
        <p>Hardware und Software</p>
    </div>
    <div id="controls" class="container-fluid">
        <form method="get">
            <div class="row">
                <div class="col-lg-2">
                    <input type="text" placeholder="Suchen" name="filter" />
                </div>
                <div class="col-lg-2">
                    <select name="category">
                        <option value="">Alle Kategorien</option>
                        <option value="PCs">PCs</option>
                        <option value="Bildschirme">Monitore</option>
                        <option value="Mäuse">Maus</option>
                        <option value="Laptops">Laptops</option>
                        <option value="Tastatur">Tastatur</option>
                        <option value="Headset">Headset</option>
                        <option value="Lautsprecher">Lautsprecher</option>
                        <option value="Drucker">Drucker</option>
                        <option value="Festplatten">Festplatten</option>
                        <option value="Beamer">Beamer</option>
                        <option value="Software">Software</option>
                        <option value="Webcam">Webcam</option>
                        <option value="Switch">Switch</option>
                        <option value="Router">Router</option>
                        <option value="Netzwerkkarte">Netzwerkkarte</option>
                        <option value="Wartung">Wartung</option>
                        <option value="Bildbearbeitung">Bildbearbeitung</option>
                        <option value="Netzwerkaufbau">Netzwerkaufbau</option>
                        <option value="Schulung">Schulung</option>
                    </select>
                </div>
                <div class="col-lg-2">
                    <select name="sortieren">
                        <option value="">Keine Sortierung</option>
                        <option value="ASC">Preis aufsteigend</option>
                        <option value="DESC">Preis absteigend</option>
                    </select>
                </div>
                <div class="col-lg-6">
                    <button type="Submit" name="Abschicken">Anwenden</button>
                </div>
            </div>
        </form>
    </div>

    <div style="padding-left: 12px;padding-right: 12px;" class="container-fluid">
        <div id="products" class="masonry">
            <?php
            require_once '../../model/Product.php';
            $products = [];

            if(isset($_GET["Abschicken"])){
                
                $filter = "";
                $category = null;
                $sortieren = null;
                if(isset($_GET["filter"])){
                    
                    $filter = $_GET["filter"];
                } 
                if(isset($_GET["category"])){
                    
                    $category = $_GET["category"];
                    
                } else if(isset($_GET["sortieren"])){
                    $sortieren = $_GET["sortieren"];
                } 
                    $products = Product::getFiltered($filter, $category, $sortieren);
            } else {
                $products = Product::getAll();
               
            }
            if($products !== null){
                foreach($products as $row){
            ?>
            <div class="item">
                <a href="detail/index.php?id=<?php echo $row->id; ?>">
                    <div class="product">
                        <div class="pimage">
                            <!-- Product image -->
                            <img src="data:image/jpeg;base64,<?php echo base64_encode($row->productImage); ?>" class="img-responsive" />
                        </div>
                        <div class="description">
                            <h3><?php echo $row->name; ?></h3>
                            <p><?php echo $row->price; ?> €</p>
                        </div>
                    </div>
                </a>
            </div>
            <?php
                }
            } else {
                echo "Keine Angebote gefunden";
            }
            ?>
        </div>
    </div>
</div>

<?php
    require_once "../../footer.php";
?>

<script>

    // Scroll
    $('.nav-link').click(function() {
        var sectionTo = $(this).attr('href');
        $('html, body').animate({
            scrollTop: $(sectionTo).offset().top
        }, 1500);
    });

    // Active nav
    $(".nav .nav-link").on("click", function(){
        $(".nav").find(".active").removeClass("active");
        $(this).addClass("active");
    });
    
    // Fill nav color on sroll
    $(function () {
        $(document).scroll(function () {
            var $nav = $("#navbarResponsive");
            $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
            var $nav2 = $("#cart-nav");
            $nav2.toggleClass('scrolled', $(this).scrollTop() > $nav2.height());
        });
    });

</script>
</body>
</html>