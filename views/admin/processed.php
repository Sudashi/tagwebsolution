<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Tag Web Solution</title>

    <link rel="shortcut icon" href="../../res/favicon.ico" type="image/x-icon">
    <style>@import url('https://fonts.googleapis.com/css?family=Ubuntu');</style>
    <style>@import url('https://fonts.googleapis.com/css?family=Montserrat');</style>

    <!-- Custom CSS -->
    <style>
      @import "../../global.css";
      @import "style.css";
    </style>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

  </head>
<?php
require_once "../../model/User.php";
require_once "../../model/Order.php";
 if(isset($_POST["processed"])){
   Order::process($_POST["processed"], true);
 }
?>
  <body data-spy="scroll" data-target=".navbar" data-offset="50">
    <!-- Navigation -->
    <nav style="width: 100vw !important;padding: 0px;" class="navbar navbar-expand-lg navbar-light fixed-top shadow-sm">
      <div style="width: 100vw !important;padding: 0px;" class="container-fluid">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>    
    
        <div style="width: 100vw !important;" class="collapse navbar-collapse order-2" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="../index.php">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>

            <?php
            session_start();
            if(isset($_SESSION['userid'])) {
              ?>
              <li class="nav-item">
                <a class="nav-link active" href="../shop/index.php">Shop</a>
              </li>
              <?php 
              }
              ?>
            <li class="nav-item">
              <a class="nav-link active" href="../faq/ask/">FAQ</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container-fluid products">
        <div class="row">
              <div class="col-lg-8">
                
              <div id="header">
                  <h1>Bestellungen</h1>
                  <button type="button" class="btn btn-primary btn-lg shadow-sm" onclick="window.location.href='index.php'">Unbearbeitete Bestellungen</button>
                </div>
             
                  <?php
                      
                      $data = Order::getProccesed(true);
                      $orders = [];
                      $lastId = 0;
                      
                      // Get Unprocessed orders as bundle
                     
                      foreach($data as $d){
                        if($lastId == $d->bestellungID){
                          continue;
                        }
                        $lastId = $d->bestellungID;
                        $orders[] = Order::getWholeOrder($d->bestellungID);
                      }

                      // Display $order Array
                      foreach($orders as $order){
                        echo "<div class='product'>";
                        $besteller = User::get($order[0]->bestellungID);
                        echo "<h3>$besteller->vorname $besteller->nachname - $besteller->email</h3>";
                        echo "<table class='product_table'>";
                        echo "<tr>";
                          echo "<th>";
                          echo "Produkt";
                          echo "</th>";
                          echo "<th>";
                          echo "Preis";
                          echo "</th>";
                          echo "<th>";
                          echo "Menge";
                          echo "</th>";
                          echo "<th>";
                          echo "Gesamt";
                          echo "</th>";
                        echo "</tr>";

                        $menge = 0;
                        $gesamt = 0;
                        foreach($order as $item){

                          require_once "../../model/Product.php";
                          $product = Product::get($item->produktID);

                          require_once "../../model/User.php";
                          // tbl_product = presi

                          echo "<tr>";
                            echo "<td>";
                            echo $product->name;
                            echo "</td>";
                            echo "<td>";
                            echo $product->price;
                            echo " €</td>";
                            echo "<td>";
                            echo $item->menge;
                            echo " Stück</td>";
                            echo "<td>";
                            $gesamtItem = ($product->price*$item->menge);
                            echo number_format((float)$gesamtItem, 2, '.', '');
                            echo " €</td>";
                          echo "</tr>";
                          $menge += $item->menge;
                          $gesamt += ($product->price*$item->menge);
                        }
                 
                        echo "<tr><td>Insgesamt</td>";
                        echo "<td></td>";
                        echo "<td>$menge Stück</td>";
                        echo "<td>".number_format((float)$gesamt, 2, '.', '')." €</td>";
                        echo "</tr>";
                        echo "</table>";
                        echo "<form action='".$_SERVER["PHP_SELF"]."' method='POST'>";
                        echo "<input type='hidden' name='processed' value='".$order[0]->bestellungID."'>";
                        echo "<button class='done'>Unerledigt</button>";
                        echo "</form>";
                        echo "</div>";
                      }

                  ?>
              </div>
        </div>
    </div>
	  
    <script>

        // Scroll
        $('.nav-link').click(function() {
            var sectionTo = $(this).attr('href');
            $('html, body').animate({
                scrollTop: $(sectionTo).offset().top
            }, 1500);
        });

        // Active nav
        $(".nav .nav-link").on("click", function(){
            $(".nav").find(".active").removeClass("active");
            $(this).addClass("active");
        });
		
        // Fill nav color on sroll
        $(function () {
          $(document).scroll(function () {
          var $nav = $("#navbarResponsive");
          $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
          });
        });

    </script>

  </body>

</html>