<!DOCTYPE html>
<html lang="de">
<?php
session_start();

require "model/User.php";
header('Location: websiteoffline.html');


?>

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Tag Web Solution</title>
    <link rel="shortcut icon" href="res/favicon.ico" type="image/x-icon">
    <style>@import url('https://fonts.googleapis.com/css?family=Ubuntu');</style>
    <style>@import url('https://fonts.googleapis.com/css?family=Montserrat');</style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

  
    <!-- Custom CSS -->
    <style>
      @import "global.css";
    </style>
  </head>

  <body data-spy="scroll" data-target=".navbar" data-offset="50">
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top p-0">
      <div class="container-fluid p-0">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>    
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="#home">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#work">Aufgaben</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#about">Unternehmen</a>
            </li>
            <?php

         

            if(isset($_SESSION["userid"])){
              $user = User::get($_SESSION["userid"]);

              if($user->admin){
                ?>

<li class="nav-item">
              <a class="nav-link" href="views/admin">Admintools</a>
            </li>
                <?php
              }
              ?>

<li class="nav-item">
              <a class="nav-link" href="views/shop/index.php">Shop</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="views/faq/index.php">FAQ</a>
            </li>
              <?php
           } else {
              ?>
 <li class="nav-item">
              <a class="nav-link" href="views/login/index.php">Log In</a>
            </li>
              <?php


           }

    
           
            ?>
          </ul>
        </div>
      </div>
    </nav>



    <!-- Bild-Slider -->

    <div id="home" class="carousel slide" data-ride="carousel">

      <ul class="carousel-indicators">

        <li data-target="#home" data-slide-to="0" class="active"></li>

        <li data-target="#home" data-slide-to="1"></li>

      </ul>

      <div class="carousel-inner">

        <div class="carousel-item active">

          <div class="row">

            <div class="col-lg-12" id="slider_1">

                <div class="carousel-caption">

                  <h1>Online Shop</h1>

                  <p>Sie suchen nach Hardware oder Software? Dann besuchen Sie unseren Shop und entdecken Sie spannende neue Dinge. Wir haben alles, was das Herz begehrt.</p>

                  <button type="button" class="btn btn-primary btn-lg shadow-sm" onclick="window.location.href='views/shop/index.php'">Jetzt shoppen</button>

                </div>

            </div>

          </div>

        </div>

        <div class="carousel-item">

            <div class="row">

                <div class="col-lg-12" id="slider_2">

                    <div class="carousel-caption">

                      <h1>Häufige Fragen</h1>

                      <p>Haben Sie Fragen? Dann besuchen Sie doch unsere FAQ Seite. Stellen Sie eine Frage oder stöbern Sie in den bereits vorhandenen Fragen.</p>

                      <button type="button" class="btn btn-primary btn-lg shadow-sm" onclick="window.location.href='views/faq/index.php'">Zum FAQ</button>

                    </div>

                </div>

            </div>

        </div>

      </div>

      <a class="carousel-control-prev" href="#home" data-slide="prev">

        <span class="carousel-control-prev-icon"></span>

      </a>

      <a class="carousel-control-next" href="#home" data-slide="next">

        <span class="carousel-control-next-icon"></span>

      </a>

    </div>



  <!-- Page Content -->

	<div class="container-fluid">

		<div id="work" class="row">

		  <div class="col-lg-4">

        <div class="wrapper one">

          <div class="wwd_text">

            <h1>Preiswert<br>und stabil</h1>

            <hr align="left">

            <p><br>Was wäre ein Computer ohne Software? Wir haben alles was Sie benötigen: Von Windows bis Office. Die wichtigste Software, die jeder PC braucht, stellen wir Ihnen preiswert zur Verfügung. Bei Problemen oder Beratung steht Ihnen unser Team bereit.</p>

          </div>

        </div>

		  </div>

		  <div class="col-lg-4">

        <div class="wrapper two">

          <div class="wwd_text">

            <h1>Schnell<br>und elegant</h1>

            <hr align="left">

            <p><br>Ob Maus, Tastatur oder ein Computer, wir habe alles was das Herz begehrt. Mit einem Sortiment von über 30 Produkte sind wir Ihre Anlaufstelle für Hardware. Bei Fragen oder Beratung stehen wir Ihnen gerne zur Verfügung.</p>

          </div>

        </div>

		  </div>

		  <div class="col-lg-4">

        <div class="wrapper three">

          <div class="wwd_text">

            <h1>Hilfe und<br>Support</h1>

            <hr align="left">

            <p><br>Die Leistung, Reaktionszeit und Verfügbarkeit unseres IT-Services richtet sich konsequent nach Ihnen. Sie erhalten eine eigene Telefon-Hotline für den besten individuellen Support. Wir bieten auch diverse Schulungen für verschiedenste Software an.</p>

          </div>

        </div>

		  </div>

		</div>

	</div>



  <div class="container-fluid">

    <div class="row newsletter">

      <div class="newsletter_text col-lg-12">

		    <h1><img width="120px" style="margin-right: 2vh;" src="res/newspaper.svg" />Newsletter</h1>

        <p>Unser Newsletter bietet Ihnen eine gute Übersicht über derzeitige Angebote und die neuesten Produkte. Holen Sie sich den Newsletter und seien Sie immer auf dem neuesten Stand über unser Unternehmen.</p>

        <form>

            <div class="row text-center">

              <div class="col-lg-12">

                <input id="nws_s" type="submit" class="btn btn-primary btn-lg shadow-sm" value="Abonnieren">

              </div>

          </div>

        </form>

      </div>

    </div>

  </div>



  <div class="container-fluid">

    <div id="about" class="row about">

	  <!-- <div class="col-lg-2 bg-white"></div> -->

      <div class="col-lg-12 bg-white about_container">

        <div class="about_text">

          <h1 style="margin-left: -10px">Das Unternehmen</h1>

		      <hr align="left" style="height: 0.7vh !important;" /><br><br>

          <p>Wir sind Ihr Experte in Sachen IT-Produkte und IT-Services für Unternehmen und Kommunen. Sie finden bei uns perfekt abgestimmte IT-Lösungen mit maximaler Sicherheit für jede Unternehmensgröße. Als vertrauensvoller Partner realisieren wir Projekte in ganz Österreich und auch dem Ausland.</p>

          <br><p>Doch was sind die richtigen Produkte für ihr Unternehmen? Mit unserer professionellen Beratung helfen wir Ihnen dabei diese zu finden. Kontaktieren Sie dazu einfach unseren Kundensupport, welcher immer für Sie bereit ist. Wurden Sie beraten, dann steht Ihnen in unserem Online Shop eine riesige Auswahl an Produkten zur Verfügung. Oder wollen Sie doch lieber eine Website, welche Ihr Unternehmen so präsentiert, wie Sie es wünschen?</p>

          <br><p>Dann ist unser Unternehmen genau dass, was Sie suchen. Denn wir erstellen einzigartige Websites nach ihren Wünschen, verkaufen Hardware und bieten zahlreiche Dienstleistungen.</p>

        </div>

      </div>

	  <!-- <div class="col-lg-2 bg-white"></div> -->

    </div>

  </div>



  <?php

    require_once "footer.php";

  ?>



    <script>



        // Scroll

        $('.nav-link').click(function() {

            var sectionTo = $(this).attr('href');

            $('html, body').animate({

                scrollTop: $(sectionTo).offset().top

            }, 1500);

        });



        // Active nav

        $(".nav .nav-link").on("click", function(){

            $(".nav").find(".active").removeClass("active");

            $(this).addClass("active");

        });

		

        // Fill nav color on sroll

        $(function () {

          $(document).scroll(function () {

          var $nav = $("#navbarResponsive");

          $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());

          });

        });



    </script>



  </body>



</html>

